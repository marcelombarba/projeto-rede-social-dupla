import {http} from '../config/http'



// User verbs
const getUsers = (data) => http.get('/user')
const deleteUser = (id) => http.delete(`/user/${id}`)
const postUser = (data) => http.post('/user', data)
const patchUser = (id, data) => http.patch(`/user/${id}`, data)



export {
    getUsers,
    deleteUser,
    postUser,
    patchUser
}